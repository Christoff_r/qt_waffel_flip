import QtQuick 2.15
import QtQuick.Controls
import QtMultimedia

Item {
    Rectangle {
        width: Window.width
        height: Window.height
        color: "transparent"

        AnimatedImage {
            id: background
            source: "qrc:/Images/animatedBG.gif"

            anchors.fill: parent
        }

        Rectangle {
            width: Window.width
            height: Window.height
            color: "transparent"

            MouseArea
            {
                anchors.fill: parent
                onPressed: themeSong.play()
            }
            MediaPlayer {
                loops: -1
                id: themeSong
                source: "qrc:/sounds/theme.mp3"
                audioOutput: AudioOutput {volume: 0.1}
            }

        }

    }
}
