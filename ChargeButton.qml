import QtQuick 2.15
import QtQuick.Controls
import QtMultimedia

Item
{
    signal chargeButtonSignalSend()
    signal chargeButtonSignalSendRelesed()

    Rectangle
    {
        width: 100; height: 100
        x: Window.width - width; y: 200
        color: "green"
        id: chargeButton
        border.color: "black"
        border.width: 5
        radius: 10

        Text
        {
            text: "BLAST WAFFLE!"
            anchors.centerIn: parent
            fontSizeMode: Text.Fit
        }

        MouseArea
        {
            anchors.fill: parent
            onPressed: {chargeButtonSignalSend(); strechSound.play(); buttonscl.start()}
            onReleased: {chargeButtonSignalSendRelesed(); strechSound.stop(); blastOff.play(); buttonscl.stop(); chargeButton.width = 100; chargeButton.height = 100}
        }

        SoundEffect
        {
            id: strechSound
            source: "qrc:/sounds/stretch.wav"
            loops: SoundEffect.Infinite
        }

        SoundEffect
        {
            id: blastOff
            source: "qrc:/sounds/blasoff.wav"
        }

        Timer {
            id: buttonscl
            interval: 50; repeat: true; running: false

            function collision() {
                if(true)
                    return true
                return false
            }

            onTriggered: {
                if(chargeButton.width <= 200)
                {
                    chargeButton.width = chargeButton.width + 1
                    chargeButton.height = chargeButton.height + 1
                }
                if(chargeButton.width >= 200)
                {
                    strechSound.stop()
                }
            }
        }
    }
}
