import QtQuick 2.15
import QtQuick.Controls
import QtMultimedia

Item {
    Rectangle {
        width: Window.width
        height: Window.height
        color: "transparent"

        AnimatedImage {
            id: forground
            source: "qrc:/Images/animatedFG.gif"

            anchors.fill: parent
        }
    }
}
