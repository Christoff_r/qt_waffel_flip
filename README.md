# Qt Kahoot :trophy:
![banner](https://imgs.search.brave.com/CBVADQfLgJFgiQ3fYLS0MjP5rZpu8WqiESs5gLvYz8M/rs:fit:468:394:1/g:ce/aHR0cHM6Ly9tZWRp/YS5naXBoeS5jb20v/bWVkaWEvbWhYejZi/aHd3VXZMeS9naXBo/eS5naWY.gif)

![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen)
![language](https://img.shields.io/badge/language-c%2B%2B-blue)
![game](https://img.shields.io/badge/game-Waffle%20Flip-orange)
![gui-editor](https://img.shields.io/badge/gui-Qt-brightgreen)

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [License](#license)


## Background

This project is a small game about flipping a waffle. Its made with the Qt QML framework.

This project was made by Rasmus & Christoffer.

See [usage](#usage) for more details.

## Install
Below is a short description on how to install the program along with some [dependencied](#dependencies).

### Dependencies

The program is made with Qt and is needed to be [installed](https://www.qt.io/download) to compile and run.

### Clone from GitLab

Navigate to were you want the repository to be. Open the `CLI` and type the following:

```
https://gitlab.com/Christoff_r/qt_waffel_flip.git
```

## Usage

Open the Qt Creater and open the `WaffelFlip.pro` file. 

### Compile & Run

When the project is open, firt press `Clt + b` to build, then press `Clt + r` to run the program. 

### Interact

To play the game, simple press and hold the green button for any amount fo time you would like. When you release the button the waffle flips. The goal land the waffle as seen in the picture.


<img src="screenPrint/screenPrint.png">

## Contributing

If you have any tips, sugestions or things like that feel free to send me a message :speech_balloon:, although I might me slow to respond :sweat_smile:

## License

`UNLICENSED`
