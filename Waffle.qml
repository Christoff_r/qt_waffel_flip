import QtQuick 2.15
import QtMultimedia

//       __________
//      | |_|_|_|_|_| |
//      | |_|_|_|_|_| |
//      | |_|_|_|_|_| |
//      | |_|_|_|_|_| |
//      | |_|_|_|_|_| |
//      ------------

Item {
    property double randWafPos: 0
    property double rotationCharge: 0
    property int charge: 0
    property bool playWow: false
    property bool playFlop: false

    MediaPlayer
    {
        id: flop
        source: "qrc:/sounds/flop.mp3"
        audioOutput: AudioOutput {volume: 1}
    }

    SoundEffect
    {
        id: swoosh
        source: "qrc:/sounds/swoosh.wav"
        loops: SoundEffect.Infinite
    }

    MediaPlayer {
        id: wow
        source: "qrc:/sounds/wow.mp3"
        audioOutput: AudioOutput {volume: 1}
    }

    //Check rotation is valid
    function checkRotation()
    {
        if(waffle.rotation >= 175 && waffle.rotation <= 185 || waffle.rotation >= 355 && waffle.rotation <= 360 || waffle.rotation >= 0 && waffle.rotation <= 5)
        {
            if(playWow)
            {
                wow.play()
                playWow = false
            }
        }
    }

    function startCharge()
    {
        startChargeTimer.running = true
    }

    function releaseCharge()
    {
        startChargeTimer.running = false

        //out of bounds
         if(waffle.y -charge < 0)
         {
            waffle.y = 0
            charge = Window.height - tableTest.height*2
         }
         else
         {
            waffle.y = waffle.y -charge
         }

        randWafPos = Math.random() * Window.width
        waffleAnimator.start()
        rotationCharge = charge
        charge = 0

    }

    Timer {
        id: startChargeTimer
        interval: 5; repeat: true; running: false

        function collision() {
            if(true)
                return true
            return false
        }

        onTriggered: {
            charge += 5

        }
    }

    signal chargeButtonSignalRecived()
    onChargeButtonSignalRecived: {startCharge()}

    signal chargeButtonSignalReleasedRecived()
    onChargeButtonSignalReleasedRecived: {releaseCharge(); swoosh.play(); playFlop = true; playWow = true}

    Rectangle {
        id: tableTest
        width: Window.width
        height: 100
        x: Window.width - width
        y: Window.height - height
        color: "brown"
        border.color: "black"
        border.width: 5
        radius: 0

        Image {
            id: tableTex
            source: "qrc:/Images/TableTex.jpg"
            anchors.fill: parent
        }
    }

    Rectangle {
        id: waffle
        y: tableTest.y - tableTest.height*1.4
        x: 400
        width: 100
        height: 150
        color: "transparent"
        radius: 10
        border.color: "black"
        border.width: 5
        transform: Rotation

        Image {
            id: wafflepng
            source: "qrc:/Images/Waffle.png"

            anchors.fill: parent
        }

        SequentialAnimation {
            id: waffleAnimator
                running: false
                NumberAnimation { target: waffle; property: "x"; to: randWafPos ; duration: charge *3.2 }

            }

        //GRAVITY
        Timer {
            interval: 5; repeat: true; running: true

            function gravity() {

                return true
            }

            onTriggered: {
                if(waffle.y + waffle.height >= tableTest.y + tableTest.height/10)
                {
                    if(playFlop)
                    {
                        swoosh.stop()
                        flop.play()
                        playFlop = false;
                    }
                }
                else
                {
                    waffle.y = waffle.y + 5
                }

            }
        }
        //ROTATION
        Timer {
            interval: 5; repeat: true; running: true

            function rotation() {

                return true
            }

            onTriggered: {
                if(waffle.y + waffle.height >= tableTest.y + tableTest.height/10)
                {
                    checkRotation();
                }
                else
                {
                    waffle.rotation = parseFloat(waffle.rotation) + rotationCharge / 10
                }
                if(waffle.rotation >= 360)
                {
                    waffle.rotation = 0
                }

            }
        }
    }
}

