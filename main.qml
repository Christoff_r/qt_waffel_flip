import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 1000
    height: 1000
    visible: true
    title: qsTr("Waffle Flip!")

    Background{}

    ChargeButton
    {
       id: chargeButton
       onChargeButtonSignalSend: waffle.chargeButtonSignalRecived()
       onChargeButtonSignalSendRelesed: waffle.chargeButtonSignalReleasedRecived()
    }

    Waffle
    {
        id: waffle
    }

    Forground{}
}
